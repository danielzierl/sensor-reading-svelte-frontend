import { col } from "./src/lib/colors";
// const colors_custom = require("./src/lib/colors");
/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    fontFamily: {
      sans: ["Fira Mono", "monospace"],
    },
    extend: { colors: col },
  },
  plugins: [],
  darkMode: "class",

  // purge:["./src"]
};
