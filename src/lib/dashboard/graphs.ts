import type { SensorReading } from "$lib/types/tempratureReading.interface";
import { Chart } from "chart.js/auto";
import type { Writable } from "svelte/store";
import { col } from "$lib/colors";
import type { Readable } from "svelte/motion";
import type { TeamMeasurment } from "$lib/types/teamMeasurment.type";
import "chartjs-adapter-date-fns";
export const makeGraph = function (container: HTMLCanvasElement): Chart {
  return new Chart(container, {
    options: {
      responsive: true,
      scales: {
        y: {
          grace: 0.2,
          ticks: {
            stepSize: 0.2,
          },
        },
        x: {
          type: "time",
          position: "bottom",
        },
      },
      maintainAspectRatio: false,
      plugins: {
        legend: {
          labels: {
            color: "white",
          },
        },
      },
    },
    type: "line",
    data: {
      // labels: [],
      datasets: [],
    },
  });
};
export const setUpdateGraphWithWritable = (
  graph: Chart,
  writable: Writable<TeamMeasurment>,
  title: string,
  teamName: string,
  chart_index: number,
) => {
  writable.subscribe((all_meas) => {
    const teamMeasurments: SensorReading[] = all_meas[teamName];
    graph.data.datasets[chart_index] = {
      label: teamName + " team",
      backgroundColor: teamName,
      borderColor: teamName,
      fill: false,
      tension: 1,
      cubicInterpolationMode: "monotone",
      pointRadius: 2,
      //@ts-ignore
      data: teamMeasurments.map((dat) => {
        const date = new Date(dat.CreatedAt);
        return {
          x: date,
          y: dat.value,
        };
      }),
    };
    graph.update("none");
  });
};
