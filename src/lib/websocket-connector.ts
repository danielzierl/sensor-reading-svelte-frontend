import type { SensorReading } from "./types/tempratureReading.interface";
import * as env from "$lib/variables";
import { writable, type Writable } from "svelte/store";
import type { TeamMeasurment } from "./types/teamMeasurment.type";

let websocket_id = 0;
const websocket_mess_handlers_time_buffer: Record<number, number> = [];
let time_deltas: Record<number, number> = [];
export const min_time_delta = writable(999);
setInterval(() => {
  time_deltas = [];
}, 5000);

export const connectToSocket = (
  readings: Writable<TeamMeasurment>,
  type: string,
  time_frame_hours: number,
  team: "red" | "yellow" | "pink" | "blue" | "black" | string,
) => {
  const websocket_id_local = websocket_id;
  const queryParams: Record<string, string> = {
    team,
    n_results: "100",
    type,
    time_frame_hours: time_frame_hours.toString(),
    Auth: `${localStorage.getItem("Auth")}`,
  };
  const queryString = Object.keys(queryParams)
    .map((key) => `${key}=${encodeURIComponent(queryParams[key])}`)
    .join("&");
  const socket = new WebSocket(
    `${env.WEB_SOCKET_PROTOCOL}://${env.API_URL}/readings?${queryString}`,
  );
  socket.onopen = () => {
    socket.send("PLS SEND");
    console.log("[socket] connected");
  };
  socket.onclose = () => {
    console.log("[socket] closed");
  };
  const messageHandler = (event: MessageEvent) => {
    if (websocket_mess_handlers_time_buffer[websocket_id_local]) {
      time_deltas[websocket_id_local] =
        Date.now() - websocket_mess_handlers_time_buffer[websocket_id_local];
    }
    websocket_mess_handlers_time_buffer[websocket_id_local] = Date.now();
    updateMinDeltaCallback();
    readings.update((data) => {
      const newReading = {
        ...data,
        [team]: JSON.parse(event.data),
      };
      return newReading;
    });
    setTimeout(() => {
      socket.send("PLS SEND");
    }, 1000);
  };
  socket.onmessage = messageHandler;

  websocket_id++;
  return socket;
};
function updateMinDeltaCallback() {
  min_time_delta.set(Math.min(...Object.values(time_deltas)));
}
