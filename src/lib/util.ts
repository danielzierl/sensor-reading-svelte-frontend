import type { SensorReading } from "./types/tempratureReading.interface";

export function capitalize(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
export function getMaxArray(array: SensorReading[]) {
  const vals: number[] = array.map((v) => v.value);
  return Math.max(...vals);
}
export function getMinArray(array: SensorReading[]) {
  const vals: number[] = array.map((v) => v.value);
  return Math.min(...vals);
}
export function getAverageArray(array: SensorReading[]) {
  const vals: number[] = array.map((v) => v.value);
  return vals.reduce((a, b) => a + b, 0) / vals.length;
}
