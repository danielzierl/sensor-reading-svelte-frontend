import * as env from "$lib/variables";
export async function verifyToken(token: string) {
  const resp = await fetch(
    `${env.HTTP_PROTOCOL}://${env.API_URL}/verify?token=${token}`,
  );
  if (resp.status !== 200) {
    return false;
  }
  const data = await resp.json();
  return data.authenticated;
}
