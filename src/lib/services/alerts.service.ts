import type { Writable } from "svelte/store";
import * as env from "$lib/variables";
export const getAlerts = (alerts: Writable<any>) => {
  const socket = new WebSocket(
    `${env.WEB_SOCKET_PROTOCOL}://${env.API_URL}/alerts`,
  );
  socket.onopen = () => {
    console.log("alert [socket] connected");
  };
  socket.onclose = () => {
    console.log("alert [socket] closed");
  };
  const messageHandler = (event: MessageEvent) => {
    alerts.set(JSON.parse(event.data));
    setTimeout(() => {
      socket.send("OK");
    }, 5000);
  };
  socket.onmessage = messageHandler;

  return socket;
};
