export type SensorReading = {
  value: number;
  CreatedAt: string;
};
