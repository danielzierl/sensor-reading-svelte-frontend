import type { SensorReading } from "./tempratureReading.interface";

export type TeamMeasurment = {
  black: SensorReading[];
  blue: SensorReading[];
  pink: SensorReading[];
  yellow: SensorReading[];
  red: SensorReading[];
};
