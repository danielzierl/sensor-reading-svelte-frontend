export let fetchAllTemps = async (): Promise<any[] | []> => {
  try {
    const resp = await fetch("http://localhost:3000/temps");
    const data = await resp.json();
    console.log("succ fetched data ", data);
    return data;
  } catch (err) {
    console.log("Error fetching temps ", err);
    return [];
  }
};
