import { API_URL, HTTP_PROTOCOL } from "$lib/variables";

export async function loadLogs() {
  return new Promise((resolve) => {
    fetch(`${HTTP_PROTOCOL}://${API_URL}/logs`)
      .then((res) => res.json())
      .then((data) => resolve(data));
  });
}
