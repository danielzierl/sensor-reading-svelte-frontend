import { goto } from "$app/navigation";
import * as env from "$lib/variables";
import { writable } from "svelte/store";
import * as jose from "jose";

export type faceResponse = {
  prediction: string;
  confidence: number;
  token: string;
};

export const loginFailed = writable<boolean>(false);
export async function login(username: string, password: string) {
  const resp: Response = await fetch(
    `${env.HTTP_PROTOCOL}://${env.API_URL}/login-base`,
    {
      method: "POST",
      body: JSON.stringify({ user_name: username, password: password }),
    },
  );
  const data = await resp.json();
  if (resp.status !== 200) {
    loginFailed.set(true);
    return;
  }
  loginFailed.set(false);
  redirect_and_set_name(data.token);
}
export async function loginFaceCallback(faceServiceResponse: Response) {
  if (faceServiceResponse.status !== 200) {
    loginFailed.set(true);
    return;
  }
  const data: faceResponse = await faceServiceResponse.json();
  loginFailed.set(false);
  console.log(data);
  redirect_and_set_name(data.token);
}
function redirect_and_set_name(token: string) {
  localStorage.setItem("Auth", token);
  const decoded_token: Record<string, string> = jose.decodeJwt(token);
  localStorage.setItem("Username", decoded_token.user_name);
  goto("/");
}
