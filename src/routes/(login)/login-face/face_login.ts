import { get, type Writable } from "svelte/store";
import * as env from "$lib/variables";
import { loginFaceCallback } from "../loging-in";

export const setupFace = function (video: HTMLVideoElement) {
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: false })
    .then(function (stream) {
      video.srcObject = stream;
      video.play();
    })
    .catch(function (err) {
      console.log(`An error occurred: ${err}`);
    });
};
export async function startTakingPictures(
  images: Writable<string[]>,
  blocked: Writable<boolean>,
  video: HTMLVideoElement,
) {
  blocked.set(true);
  images.set([]);
  console.log("click");
  const canvas_holder = document.querySelector("#canvas-holder");
  if (!canvas_holder) {
    console.log("no canvas holder");
    return;
  }
  Array.from(canvas_holder?.children).forEach((canvas) => {
    canvas.remove();
  });
  console.log(canvas_holder);
  for (let i = 0; i < 8; i++) {
    const canvas: HTMLCanvasElement = document.createElement("canvas");
    canvas.classList.add("w-full");
    canvas.classList.add("h-full");
    canvas_holder.appendChild(canvas);
    const context = canvas.getContext("2d");
    context?.drawImage(video, 0, 0, canvas.width, canvas.height);
    const imageData = canvas.toDataURL("image/png");
    images.set([...get(images), imageData]);

    await new Promise<void>((resolve) => {
      setTimeout(() => {
        resolve();
      }, 200);
    });
  }
  await sendPictures(images);
  blocked.set(false);
}
async function sendPictures(images: Writable<string[]>) {
  const resp = await fetch(`${env.HTTP_PROTOCOL}://${env.API_URL}/login-face`, {
    method: "POST",
    body: parseImagesToJSON(get(images)),
  });
  await loginFaceCallback(resp);
}
function parseImagesToJSON(images: string[]) {
  const jsonData = {
    images: images,
  };
  const imagesJson = JSON.stringify(jsonData);
  return imagesJson;
}
