FROM node:20-alpine AS build
WORKDIR /app
COPY . .
RUN npm install
# CMD ["npm","run","dev"]
RUN npm run build


FROM nginx:latest
WORKDIR /app
COPY --from=build /app/build /usr/share/nginx/html
COPY . /app
COPY nginx.conf /etc/nginx/conf.d/default.conf
